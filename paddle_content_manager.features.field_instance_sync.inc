<?php
/**
 * @file
 * paddle_content_manager.features.field_instance_sync.inc
 */

/**
 * Implements hook_field_instance_sync_defaults().
 */
function paddle_content_manager_field_instance_sync_defaults() {
  $field_data = array();

  $field_data['node-basic_page-field_sticky_side_column'] = array(
    'synced' => TRUE,
    'slave' => FALSE,
  );

  return $field_data;
}
